package com.example.examenmobile

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

 class RegisterViewModel( val registerRepository: RegisterRepository): ViewModel(){
     val model: LiveData<UiModel>
         get()=_model
     private val _model= MutableLiveData<UiModel>()

     sealed class UiModel{
         class Login(val success:Boolean): UiModel()
         object Loading:UiModel()

     }
     fun doLogin(Name: String, LastName: String, Correo:String){
         _model.value=UiModel.Loading
         val runnable= Runnable {
             _model.value= UiModel.Login(registerRepository.saveUser(Name, LastName, Correo))
         }
         Handler().postDelayed(runnable,3000)
     }


}
